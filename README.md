
     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 


Useful links
============

VK for developers: https://new.vk.com/dev

Link template for access_token:
https://oauth.vk.com/authorize?client_id=__CLIENT_ID___&display=page&redirect_uri=__URL__&scope=65540&response_type=token&v=5.52

Replace __CLIENT_ID___ and __URL__
https://vc-final-2016-pyhoster.c9users.io


Clarifai: https://developer.clarifai.com/signup/
Cloudsight: https://cloudsightapi.com/api_client_users/new
